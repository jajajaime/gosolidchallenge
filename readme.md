# Solution of the GoSolid Challenge by Jaime Lopez Avendano

- Uses Laravel 5.2.36 and yajra/laravel-datatables-oracle on the back-end.
- Uses Bootstrap, jQuery and DataTables on the front-end.
- DataTables is used for the pagination, search of employee name, and sorting.

## To-Do

- There are still some problems with the sorting of the columns "Boss Name" and "Distance from CEO," that I tried to debug but could not find the problem. My guess is with the yajra/laravel-datatables-oracle package.
- I could not think of an efficient way to calculate the number of subordinates per employee.