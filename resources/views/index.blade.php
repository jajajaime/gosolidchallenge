<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Org Chart Viewer</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>

<body>

    <div class="container">

        <table class="table table-bordered" id="employeesTable">
            <thead>
                <tr class="info">
                    <th>Employee Name</th>
                    <th>Boss Name</th>
                    <th>Distance from CEO</th>
                </tr>
            </thead>
        </table>

    </div>


    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <script>
        // Script left here for simplicity

        $(function()
        {
            // Instanciating the DataTable object with server-side processing.
            $('#employeesTable').DataTable(
            {
                "aaSorting": [],
                "iDisplayLength": 100,
                processing: true,
                serverSide: true,
                ajax: '{!! URL::to('data') !!}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'bossName', name: 'boss_name', 'searchable': false },
                    { data: 'CEOdistance', name: 'distance_from_ceo', 'searchable': false }
                ]
            });
        });
    </script>

</body>
</html>