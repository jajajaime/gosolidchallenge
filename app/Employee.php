<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Employee extends Model
{
    /**
     * Relationship between subordinate and boss on the Model itself.
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function boss()
    {
        return $this->belongsTo('App\Employee', 'bossId');
    }
}
