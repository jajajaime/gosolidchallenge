<?php


// Index route
Route::get('/', 'EmployeesController@index');

// Data route that returns JSON
Route::get('data', 'EmployeesController@data');