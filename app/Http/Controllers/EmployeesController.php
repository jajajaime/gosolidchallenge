<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Employee;
use Datatables;

class EmployeesController extends Controller
{
    /**
     * Load index page.
     * 
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }
    

    /**
     * Process datatables ajax request.
     * It uses yajra/laravel-datatables-oracle package with MIT license.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        // Query the employees ordered by their bossId for always hierarchichal data.
        // The collection is then keyed by its id for easy access to add properties.
        $employees = Employee::with('boss')
            ->orderBy('bossId')
            ->get()
            ->keyBy('id');

        // Determine distance from CEO
        $employees = $this->CEOdistance($employees);


        return Datatables::collection($employees)->make(true);
    }


    /**
     * Determine distance from CEO.
     * 
     * @param Illuminate\Database\Eloquent\Collection
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    private function CEOdistance($employees)
    {
        foreach($employees as $employee)
        {
            // Flatten the boss' name and remove the eager loaded boss.
            // This is done so less data is sent to the browser.
            $employee->bossName = $employee->boss->name;
            unset($employee->boss);

            // Special case for CEO himself. He has distance 0.
            if($employee->id == 1)
            {
                $employee->CEOdistance = 0;

                continue;
            }

            // Get the distance to the CEO from the boss and add 1.
            $employee->CEOdistance = $employees{$employee->bossId}->CEOdistance + 1;
        }


        return $employees;
    }

    
}
